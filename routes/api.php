<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
  return $request->user();
});

/*
 * Public Routes
 */
Route::get('/test', 'TestController@apiTest');

//auth routes
Route::post('/login', 'AuthController@login');

/*
 * Users Routes
 */
Route::group(['middleware' => 'auth.jwt'], function () {
  Route::post('/register', 'AuthController@register'); //edit for super admin user

  Route::Resource('reservation', 'ReservationController');
  Route::Resource('call-request', 'CallRequestController');
  Route::Resource('hotel', 'HotelController');
  Route::get('status', 'StatusController@index');

  Route::get('statistics', 'StatisticsController@index');
});
