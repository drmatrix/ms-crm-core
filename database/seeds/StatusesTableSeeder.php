<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StatusesTableSeeder extends Seeder
{
  public function run()
  {
    // statuses: just_added, spam, follow_up, new_offer, payment, finalized, canceled
    DB::table('statuses')->insert([
      'id' => 1,
      'name' => 'just_added',
    ]);

    DB::table('statuses')->insert([
      'id' => 2,
      'name' => 'spam',
    ]);

    DB::table('statuses')->insert([
      'id' => 3,
      'name' => 'follow_up',
    ]);

    DB::table('statuses')->insert([
      'id' => 4,
      'name' => 'new_offer',
    ]);

    DB::table('statuses')->insert([
      'id' => 5,
      'name' => 'payment',
    ]);

    DB::table('statuses')->insert([
      'id' => 6,
      'name' => 'finalized',
    ]);

    DB::table('statuses')->insert([
      'id' => 7,
      'name' => 'canceled',
    ]);
  }
}
