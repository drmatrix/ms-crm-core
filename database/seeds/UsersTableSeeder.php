<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
  public function run()
  {
    // statuses: just_added, spam, follow_up, new_offer, payment, finalized, canceled
    DB::table('users')->insert([
      'id' => 1,
      'name' => 'Reza Hamouleh',
      'email' => 'me@rezaha.ir',
      'phone_number' => '9334123322',
      'password' => bcrypt('im!@#Reza$%^'),
      'administrator' => 1
    ]);

    DB::table('users')->insert([
      'id' => 1000,
      'name' => 'MosaferSalamWebsite',
      'email' => 'info@mosafersalam.com',
      'phone_number' => '9151202086',
      'password' => bcrypt('Mo$afer$alaM'),
      'administrator' => 1
    ]);

    DB::table('users')->insert([
      'id' => 2,
      'name' => 'Azadmanesh',
      'email' => 'nona.ghotb@gmail.com',
      'phone_number' => '9105781720',
      'password' => bcrypt('Azadmanesh'),
      'administrator' => 0
    ]);

    DB::table('users')->insert([
      'id' => 3,
      'name' => 'Aghili',
      'email' => 'tabaran_travel@yahoo.com',
      'phone_number' => '9921437813',
      'password' => bcrypt('Aghili'),
      'administrator' => 0
    ]);
  }
}
