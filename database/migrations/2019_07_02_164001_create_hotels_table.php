<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHotelsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('hotels', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->unsignedBigInteger('wp_id')->nullable();
      $table->string('name');
      $table->unsignedInteger('free_capacity')->nullable();
      $table->string('phone_number');
      $table->binary('status');
      $table->json('details')->nullable();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('hotels');
  }
}
