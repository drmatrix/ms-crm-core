<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('reservations', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->unsignedBigInteger('wp_id')->nullable();
      $table->unsignedBigInteger('creator_user_id')->nullable();
      $table->unsignedBigInteger('assign_user_id')->nullable();
      $table->unsignedBigInteger('customer_id');
      $table->unsignedBigInteger('status_id');
      $table->unsignedBigInteger('hotel_id');
      $table->unsignedBigInteger('reservation_code')->nullable();
      $table->unsignedInteger('stay_duration')->comment('unit is NIGHTS');
      $table->date('entry_date');
      $table->date('outgoing_date');
      $table->unsignedInteger('passengers')
        ->default(1)
        ->comment('number of passengers');
      $table->unsignedInteger('board_price')->nullable();
      $table->unsignedInteger('buying_price')->nullable();
      $table->unsignedInteger('selling_price')->nullable();
      $table->dateTime('assign_time')->nullable();
      $table->text('operator_description')->nullable();
      $table->timestamps();

      $table->foreign('creator_user_id')->references('id')->on('users');
      $table->foreign('assign_user_id')->references('id')->on('users');
      $table->foreign('customer_id')->references('id')->on('customers');
      $table->foreign('status_id')->references('id')->on('statuses');
      $table->foreign('hotel_id')->references('id')->on('hotels');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('reservations');
  }
}
