<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCallRequestsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('call_requests', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->unsignedBigInteger('assign_user_id')->nullable();
      $table->unsignedBigInteger('customer_id');
      $table->unsignedBigInteger('status_id');
      $table->unsignedBigInteger('hotel_id')->nullable();
      $table->dateTime('assign_time')->nullable();
      $table->text('operator_description')->nullable();
      $table->timestamps();

      $table->foreign('assign_user_id')->references('id')->on('users');
      $table->foreign('customer_id')->references('id')->on('customers');
      $table->foreign('status_id')->references('id')->on('statuses');
      $table->foreign('hotel_id')->references('id')->on('hotels');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('call_requests');
  }
}
