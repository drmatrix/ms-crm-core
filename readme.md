## MS CRM Core

[![CodeFactor](https://www.codefactor.io/repository/github/drmatrix0/ms-crm-core/badge)](https://www.codefactor.io/repository/github/drmatrix0/ms-crm-core)

_MS CRM Core_ is the server side service of a Customer Relationship Management application created by Mosafer Salam Team. 

### Dependencies
- Composer
- php > 7.1.3
- Mysql

### How to install
```shell
$ git clone https://github.com/drmatrix0/ms-crm-core.git
$ cd ms-crm-core
$ composer install
$ php artisan migrate
$ php artisan db:seed
$ php artisan serve
```
