<?php

namespace App\Http\Renderers;

class ApiRenderer
{
  public function apiRender($endpointType, $code)
  {
    //default
    $res['body'] = 'ok';
    $res['status'] = 200;

    if ($code == 1) { //ok
      if ($endpointType == 'store') {
        $res['body'] = ['msg' => 'Added Successfully'];
      } elseif ($endpointType == 'update') {
        $res['body'] = ['msg' => 'Updated Successfully'];
      }
      $res['status'] = 200;
    }

    if ($code == 2) { //bad request
      $res['body'] = ['msg' => 'bad request'];
      $res['status'] = 400;
    }

    if ($code == 3) { //404
      $res['body'] = ['msg' => 'not found'];
      $res['status'] = 404;
    }

    if ($code == 4) { //403
      $res['body'] = ['msg' => 'permission denied'];
      $res['status'] = 403;
    }

    return $res;
  }
}