<?php

namespace App\Http\Controllers;

use App\Http\Renderers\ApiRenderer;
use App\Models\CallRequest;
use App\Models\Customer;
use App\Models\Status;
use App\Services\CustomerService;
use App\Services\HotelService;
use App\Services\NotificationService;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CallRequestController extends Controller
{
  protected $customerService;
  protected $renderer;

  public function __construct(CustomerService $customerService, ApiRenderer $renderer)
  {
    $this->customerService = $customerService;
    $this->renderer = $renderer;
  }

  public function index(Request $request)
  {
    $limit = $request->limit;
    $sortedBy = isset($request->sorted_by) ? $request->sorted_by : "created_at";
    $sortDir = isset($request->sort_dir) ? $request->sort_dir : 'desc';
    $callRequests = CallRequest::with(['customer', 'hotel', 'status', 'assignUser']);

    // filters
    if (isset($request->status_id)) {
      $callRequests = $callRequests->where('status_id', $request->status_id);
    }

    // fetch data
    $callRequests = $callRequests->orderBy($sortedBy, $sortDir)
      ->paginate($limit);

    return $this->sendResponse($callRequests);
  }

  public function store(Request $request)
  {
    if (isset($req->customer["id"])) {
      $customer = Customer::where('wp_id', $request->customer["id"])->first();
    }
    if (!isset($customer)) {
      $customerData = $request->customer;
      $customerData['wp_id'] = isset($customerData['id']) ? $customerData['id'] : null;
      $customer = $this->customerService->creasteNewCustomer($customerData);
    }

    $data = array_except($request, ['customer']);
    $data['wp_id'] = $data['id'];
    $data['customer_id'] = $customer->id;
    $just_added_status_id = Status::where('name', 'just_added')->first()->id;
    $data['status_id'] = $just_added_status_id;
    CallRequest::create($data->toArray());
    $res = $this->renderer->apiRender('store', 1);
    return $this->sendMessage($res);
  }

  public function show($id)
  {
    $callRequest = CallRequest::find($id);
    if (!$callRequest) {
      $err = $this->renderer->apiRender('show', 3);
      return $this->sendMessage($err);
    }
    $result = $callRequest;
    $result['customer'] = $callRequest->customer()->first();
    $result['hotel'] = $callRequest->hotel()->first();
    $result['status'] = $callRequest->status()->first();
    $result['assign_user'] = $callRequest->assignUser()->first();
    return $this->sendResponse($result);

  }

  public function update(Request $request, $id)
  {
    $callRequest = CallRequest::find($id);
    if (auth()->user()->isAdministrator()
      || $callRequest->assignUser()->first->id == auth()->user()->id) {
      if (isset($request->assign_to_me) && $request->assign_to_me == 1) {
        $callRequest->assign_user_id = auth()->user()->id;
        $callRequest->assign_time = Carbon::now();
        $callRequest->save();
      } else {
        $callRequest->update($request->toArray());
      }
      $res = $this->renderer->apiRender('update', 1);
      return $this->sendMessage($res);
    }
    $err = $this->renderer->apiRender('update', 4);
    return $this->sendMessage($err);
  }
}
