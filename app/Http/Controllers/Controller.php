<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
  use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

  public function sendResponse($result)
  {
    $response = [
      'success' => true,
      'data' => $result,
    ];

    return response()->json($response, 200);
  }

  public function sendMessage($error)
  {
    return response()->json($error['body'], $error['status']);
  }
}
