<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller as Controller;
use App\Models\Hotel;
use App\Models\Reservation;

class StatisticsController extends Controller
{
  public function index()
  {
    return response()->json(
      [
        'count_of_reservations' => Reservation::all()->count(),
        'count_of_hotels' => Hotel::all()->count(),
        'count_of_just_added_reservations' => Reservation::where('status_id', 1)->count(),
        'count_of_finalized_reservations' => Reservation::where('status_id', 6)->count(),
      ]
    );
  }
}
