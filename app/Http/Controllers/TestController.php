<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller as Controller;
use App\Services\TestService;

class TestController extends Controller
{
  protected $service;

  public function __construct(TestService $testService)
  {
    $this->service = $testService;
  }

  public function apiTest()
  {
    return response()->json(
      [
        "status" => 'active',
        "version" => $this->service->getApplicationVersion()
      ]
    );
  }
}
