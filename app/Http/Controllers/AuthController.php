<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use JWTAuth;

class AuthController extends Controller
{
  public function register(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'phone_number' => 'required',
      'email' => 'required|string|email|max:255',
      'password' => 'required',
      'name' => 'nullable'
    ]);

    if ($validator->fails()) {
      return response()->json($validator->errors());
    }

    $user = User::create([
      'email' => $request->email,
      'password' => bcrypt($request->password),
      'name' => $request->name,
      'phone_number' => $request->phone_number,
    ]);

    return response()->json([
      'success' => true,
      'data' => $user
    ], 200);
  }

  public function login(Request $request)
  {
    $input = $request->only('email', 'password');
    $jwt_token = null;

    if (!$jwt_token = JWTAuth::attempt($input)) {
      return response()->json([
        'success' => false,
        'message' => 'Invalid Email or Password',
      ], 401);
    }

    return response()->json([
      'success' => true,
      'token_type' => 'bearer',
      'token' => $jwt_token,
      'user' => auth()->user()
    ]);
  }
}
