<?php

namespace App\Http\Controllers;

use App\Http\Renderers\ApiRenderer;
use App\Models\Reservation;
use App\Services\NotificationService;
use App\Services\ReservationService;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ReservationController extends Controller
{
  protected $reservationService;
  protected $renderer;
  protected $notificationService;

  public function __construct(
    ReservationService $reservationService,
    ApiRenderer $renderer,
    NotificationService $notificationService
  )
  {
    $this->reservationService = $reservationService;
    $this->renderer = $renderer;
    $this->notificationService = $notificationService;
  }

  public function index(Request $request)
  {
    $limit = $request->limit;
    $sortedBy = isset($request->sorted_by) ? $request->sorted_by : "created_at";
    $sortDir = isset($request->sort_dir) ? $request->sort_dir : 'desc';

    $reservations = Reservation::with(['customer', 'hotel', 'status', 'assignUser']);

    //|------------------------
    //| output filters
    //|------------------------
    if (isset($request->status_id)) {
      $reservations = $reservations->where('status_id', $request->status_id);
    }

    if (isset($request->hotel_id)) {
      $reservations = $reservations->where('hotel_id', $request->hotel_id);
    }

    if (isset($request->entry_date)) {
      $reservations = $reservations->where('entry_date', $request->entry_date);
    }

    if (isset($request->outgoing_date)) {
      $reservations = $reservations->where('outgoing_date', $request->outgoing_date);
    }

    if (isset($request->board_price_top_line)) {
      $reservations = $reservations
        ->where('board_price', '<=', $request->board_price_top_line);
    }

    if (isset($request->board_price_bottom_line)) {
      $reservations = $reservations
        ->where('board_price', '>=', $request->board_price_bottom_line);
    }
    //------------------------- this comment style is so funny :))
    // we can use a helper to write dynamic filters??

    $reservations = $reservations->orderBy($sortedBy, $sortDir)->paginate($limit);

    return $this->sendResponse($reservations);
  }

  public function store(Request $request)
  {
    $res = $this->reservationService->addNewReservationByApiRequest($request);
    $res = $this->renderer->apiRender('store', $res);
    return $this->sendMessage($res);
  }

  public function show($id)
  {
    $reservation = Reservation::find($id);
    if (!$reservation) {
      $err = $this->renderer->apiRender('show', 3);
      return $this->sendMessage($err);
    }
    $result = $reservation;
    $result['customer'] = $reservation->customer()->first();
    $result['hotel'] = $reservation->hotel()->first();
    $result['status'] = $reservation->status()->first();
    $result['assign_user'] = $reservation->assignUser()->first();
    $result['creator_user'] = $reservation->creatorUser()->first();
    return $this->sendResponse($result);
  }

  public function update(Request $request, $id)
  {
    $reservation = Reservation::find($id);
    if (isset($request->assign_to_me) && $request->assign_to_me == 1) {
      $reservation->assign_user_id = auth()->user()->id;
      $reservation->assign_time = Carbon::now();
      $reservation->save();
      $res = $this->renderer->apiRender('update', 1);
      return $this->sendMessage($res);
    } else {
      if (auth()->user()->isAdministrator() || (!is_null($reservation->assignUser()) &&
          $reservation->assignUser()->first()->id == auth()->user()->id)) {
        $reservation->update($request->toArray());

        //send notifications
        if (isset($request->status_id)) {
          if ($request->status_id == 5) {
            $customer = $reservation->customer->first();
            $body =
              "{$customer->name} عزیز
ظرفیت هتل آزادی تهران تایید شد. لطفا برای نهایی شدن رزروتان تا 15 دقیقه آینده صورت حسابتان را پرداخت کنید.

با تشکر از اعتماد شما
مسافرسلام :) ";
            $receiverPhone = [$customer->phone_number];
            $this->notificationService->sendNewNotification($body, $receiverPhone, ['sms']);
          } else if ($request->status_id == 6) {
            $customer = $reservation->customer->first();
            $body =
              "{$customer->name} عزیز
رزرو شما برای هتل آزادی تهران با موفقیت به ثبت رسید. کد پیگیری شما: {$reservation->id}
با مسافرسلام, خوش بگذرونید :)";
            $receiverPhone = [$customer->phone_number];
            $this->notificationService->sendNewNotification($body, $receiverPhone, ['sms']);
          }
        }

        $res = $this->renderer->apiRender('update', 1);
        return $this->sendMessage($res);
      } else {
        $err = $this->renderer->apiRender('update', 4);
        return $this->sendMessage($err);
      }
    }
    $err = $this->renderer->apiRender('update', 2);
    return $this->sendMessage($err);
  }
}
