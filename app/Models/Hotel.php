<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
  protected $fillable = [
    'wp_id', 'name', 'free_capacity', 'phone_number', 'status', 'details'
  ];

  /*
  * Relation methods
  */
  public function Reservations()
  {
    return $this->hasMany('App\Models\Reservation');
  }
}
