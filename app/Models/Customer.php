<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
  protected $fillable = ['wp_id', 'name', 'phone_number', 'details'];

  /*
  * Relation methods
  */
  public function Reservations()
  {
    return $this->hasMany('App\Models\Reservation');
  }
}
