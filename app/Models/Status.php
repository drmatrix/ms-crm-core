<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
  /*
  * Relation methods
  */
  public function Reservations()
  {
    return $this->hasMany('App\Models\Reservation');
  }
}
