<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
  use Notifiable;

  protected $fillable = [
    'name', 'email', 'password', 'administrator', 'phone_number'
  ];

  protected $hidden = [
    'password'
  ];

  public function isAdministrator()
  {
    return $this->administrator;
  }

  /*
   * authentication with JWT
   */
  public function getJWTIdentifier()
  {
    return $this->getKey();
  }

  public function getJWTCustomClaims()
  {
    return [];
  }


  /*
   * Relation methods
   */
  public function Reservations()
  {
    return $this->hasMany('App\Models\Reservation');
  }
}
