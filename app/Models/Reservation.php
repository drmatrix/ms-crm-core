<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
  protected $fillable = [
    'wp_id', 'creator_user_id', 'assign_user_id', 'customer_id', 'status_id', 'hotel_id', 'reservation_code',
    'stay_duration', 'entry_date', 'outgoing_date', 'passengers', 'board_price', 'buying_price',
    'selling_price', 'assign_time', 'operator_description'
  ];

  /*
  * Relation methods
  */
  public function hotel()
  {
    return $this->belongsTo('App\Models\Hotel');
  }

  public function creatorUser()
  {
    return $this->belongsTo('App\Models\User', 'creator_user_id');
  }

  public function assignUser()
  {
    return $this->belongsTo('App\Models\User', 'assign_user_id');
  }

  public function customer()
  {
    return $this->belongsTo('App\Models\Customer');
  }

  public function status()
  {
    return $this->belongsTo('App\Models\Status');
  }
}
