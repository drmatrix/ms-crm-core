<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CallRequest extends Model
{
  protected $fillable = [
    'assign_user_id', 'customer_id', 'status_id', 'hotel_id', 'assign_time',
    'operation_description', 'source_link'
  ];

  /*
  * Relation methods
  */
  public function hotel()
  {
    return $this->belongsTo('App\Models\Hotel');
  }

  public function assignUser()
  {
    return $this->belongsTo('App\Models\User', 'assign_user_id');
  }

  public function customer()
  {
    return $this->belongsTo('App\Models\Customer');
  }

  public function status()
  {
    return $this->belongsTo('App\Models\Status');
  }
}
