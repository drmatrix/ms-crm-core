<?php

namespace App\Services;

use App\Models\Customer;
use App\Models\Hotel;
use App\Models\Reservation;
use App\Models\Status;
use App\Models\User;
use App\Services\NotificationService;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Morilog\Jalali\CalendarUtils;

class ReservationService
{
  protected $hotelService;
  protected $customerService;
  protected $reservationService;
  protected $notificationService;

  public function __construct(
    HotelService $hotelService,
    CustomerService $customerService,
    NotificationService $notificationService
  )
  {
    $this->hotelService = $hotelService;
    $this->customerService = $customerService;
    $this->notificationService = $notificationService;
  }

  public function addNewReservationByApiRequest($req)
  {
    if (isset($req->id)) { //for WP requests
      $oldReserve = Reservation::where('wp_id', $req->id)->first();
      if (isset($oldReserve)) {
        return 2;
      }
    }

    if (isset($req->reservation_code)) { //for WP requests
      $oldReserve = Reservation::where('reservation_code', $req->reservation_code)->first();
      if (isset($oldReserve)) {
        return 2;
      }
    }

    // Only WP is able to create new Hotel with sending new details
    $hotel = Hotel::where('wp_id', $req->hotel["id"])->first();
    if (!isset($hotel)) {
      $hotelData = $req->hotel;
      $hotelData['wp_id'] = $hotelData['id'];
      $hotel = $this->hotelService->createNewHotel($hotelData);
    }

    if (isset($req->customer["id"])) {
      $customer = Customer::where('wp_id', $req->customer["id"])->first();
    }
    if (!isset($customer)) {
      $customerData = $req->customer;
      $customerData['wp_id'] = isset($customerData['id']) ? $customerData['id'] : null ;
      $customer = $this->customerService->creasteNewCustomer($customerData);
    }

    // dates to georgian
    $entryDate = Carbon::createFromFormat('Y-m-d', $req->entry_date);
    $outgoingDate = Carbon::createFromFormat('Y-m-d', $req->outgoing_date);
    if ($entryDate->year < 1500) {
      $entryDate = CalendarUtils::toGregorian(
        $entryDate->year,
        $entryDate->month,
        $entryDate->day
      );
      $entryDate = Carbon::createFromDate($entryDate[0], $entryDate[1], $entryDate[2]);

      $outgoingDate = CalendarUtils::toGregorian(
        $outgoingDate->year,
        $outgoingDate->month,
        $outgoingDate->day
      );
      $outgoingDate = Carbon::createFromDate($outgoingDate[0], $outgoingDate[1], $outgoingDate[2]);
    }

    $reservationData = array_except($req, ['hotel', 'customer', 'entry_date', 'outgoing_date']);;
    $reservationData['wp_id'] = $reservationData['id'];
    $reservationData['customer_id'] = $customer->id;
    $reservationData['hotel_id'] = $hotel->id;
    $reservationData['creator_user_id'] = auth()->user()->id;
    $reservationData['entry_date'] = $entryDate->toDateString();
    $reservationData['outgoing_date'] = $outgoingDate->toDateString();
    $this->createNewReservation($reservationData->toArray());

    $body = "یک رزرو جدید ثبت شده است.
{$customer->name} - {$req->stay_duration} شب - {$hotel->name}"
    ;

    $receiverPhones = User::all('phone_number')->toArray();
    $receiverPhones = Arr::flatten($receiverPhones);
    $this->notificationService->sendNewNotification($body, $receiverPhones, ['sms']);
    return 1;
  }

  public function createNewReservation($data)
  {
    if (!isset($data['status_id'])) {
      $just_added_status_id = Status::where('name', 'just_added')->first()->id;
      $data['status_id'] = $just_added_status_id;
    }

    $reservation = Reservation::create($data);
    return $reservation;
  }
}