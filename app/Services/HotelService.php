<?php

namespace App\Services;

use App\Models\Hotel;

class HotelService
{
  public function createNewHotel($data)
  {
    if (!isset($data["status"])) {
      $data["status"] = 1;
    }
    $hotel = Hotel::create($data);
    return $hotel;
  }
}