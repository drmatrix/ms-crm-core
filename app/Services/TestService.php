<?php

namespace App\Services;

class TestService
{
  public function getApplicationVersion()
  {
    return env('APP_VERSION');
  }
}