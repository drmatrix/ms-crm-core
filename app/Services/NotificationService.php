<?php

namespace App\Services;

use App\Helpers\SmsHelper;

class NotificationService
{
  protected $smsHelper;

  public function __construct(SmsHelper $smsHelper)
  {
    $this->smsHelper = $smsHelper;
  }

  public function sendNewNotification($body, $receiver, $channels = ['sms'])
  {
    if (in_array('sms', $channels)) {
      $this->sendNotificationViaSms($body, $receiver);
    }
    if (in_array('email', $channels)) {
      $this->sendNotificationViaEmail();
    }
    return true;
  }

  protected function sendNotificationViaSms($body, $receiver)
  {
    $this->smsHelper->send($body, $receiver);
    return true;
  }

  public function sendNotificationViaEmail()
  {
    return true;
  }
}
