<?php
namespace App\Helpers;

use Kavenegar\KavenegarApi;

class SmsHelper
{
  protected $kavenegarApiKey = '677458556E3278327573414C34525135695A387343496A6346796564753132704C5A79627A6A72735455773D';
  protected $sender = '30004681';

  public function send($body, $receiver)
  {
    $provider = new KavenegarApi($this->kavenegarApiKey);
    $receiverString = implode(",", $receiver);
    $provider->Send($this->sender, $receiverString, $body);
    return true;
  }
}
